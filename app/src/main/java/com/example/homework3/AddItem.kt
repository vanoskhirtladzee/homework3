package com.example.homework3

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.add_item_layout.*
import java.util.*

class AddItem : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_item_layout)
        init()
    }

    private fun init(){
        addItemButtonn.setOnClickListener{
            items()
        }
    }

    private fun items(){
        val itemModel = ItemModel(
            R.mipmap.pic,
            titleEditText.text.toString(),
            descriptionEditText.text.toString(),
            Date().toString()
        )

        val intent = intent
        intent.putExtra("iteModel", itemModel)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }


}