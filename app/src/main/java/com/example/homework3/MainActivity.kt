package com.example.homework3


import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.add_item_layout.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private val addItemCode = 1
    val items = ArrayList<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        addItemButton.setOnClickListener {
            val intent = Intent(this, AddItem::class.java)
            startActivityForResult(intent, addItemCode)
        }

        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items, this)
        recyclerView.adapter = adapter

        items.add(ItemModel(R.mipmap.pic, "Title1", "Description1", Date().toString()))
        items.add(ItemModel(R.mipmap.pic, "Title2", "Description2", Date().toString()))
        items.add(ItemModel(R.mipmap.pic, "Title3", "Description3", Date().toString()))
        items.add(ItemModel(R.mipmap.pic, "Title4", "Description4", Date().toString()))
    }


    private fun addItems(iteModel: ItemModel){
        items.add(iteModel)
        adapter.notifyItemInserted(items.size - 1)
        recyclerView.scrollToPosition(items.size - 1)
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == addItemCode && resultCode == Activity.RESULT_OK){
            val iteModel = data!!.getParcelableExtra("iteModel") as ItemModel
            addItems(iteModel)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


}
